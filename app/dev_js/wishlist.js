// Remove items from wishlist
$(".wishlist .add-to-wish").click(function() {
  const removeText = $(this).data("remove");
  $.notify({
    title: removeText
  });
  $(this)
    .closest(".product")
    .remove();
});

// Add items from recommended
$(".recommended .add-to-wish").click(function() {
  const addText = $(this).data("add");
  const removeText = $(this).data("remove");
  if (!$(this).hasClass("voted")) {
    $(this).addClass("voted");
    $.notify({
      title: addText
    });
  } else {
    $(this).removeClass("voted");
    $.notify({
      title: removeText
    });
  }
});
