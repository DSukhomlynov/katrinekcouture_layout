$("#contactForm").submit(function(e) {
  e.preventDefault();
  const _this = this;
  // поместить в callback обработчика отправки формы
  $.fancybox.open({
    src: "#contactSuccess",
    type: "inline",
    touch: false,
    smallBtn: true,
    btnTpl: {
      smallBtn: '<i class="popup-close icon-close" data-fancybox-close></i>'
    },
    afterClose: function() {
      _this.reset();
    }
  });
})
