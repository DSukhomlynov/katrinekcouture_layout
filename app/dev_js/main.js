// Lazyload
const lazyLoadInstance = new LazyLoad({
  elements_selector: ".lazy"
});

// matchHeight
$(function() {
	$('.parallax-card__title').matchHeight();
	$('.parallax-card__text').matchHeight();
  $('.product-preview__title').matchHeight();
  $('.product-img').matchHeight();
  $(".login-text").matchHeight({byRow: false});
});

// Notify customisation
$.notify.defaults({
  style: "notify",
  autoHide: true,
  autoHideDelay: 3000,
  clickToHide: true,
  hideDuration: 400
});

$.notify.addStyle("notify", {
  html: `<div>
    <div class="notify-title" data-notify-html="title"></div>
    <i class="icon-close notify-close"></i>
  </div>`
});

// Popups
$(".js-popup-callback").fancybox({
  src: "#popup_callback",
  type: "inline",
  touch: false,
  smallBtn: true,
  btnTpl: {
    smallBtn: '<i class="popup-close icon icon-cross" data-fancybox-close></i>'
  }
});

// Popovers
$('[data-toggle="popover"]').not('.contacts-btn')
  .popover({
    trigger: "manual",
    html: true,
    content: function() {
      return $(this)
        .next(".popover-content")
        .html();
    }
  })
  .on("mouseenter", function() {
    const _this = this;
    $(_this).popover("show");
    $(".popover").on("mouseleave", function() {
      setTimeout(function() {
        if (!$(_this).is(":hover")) {
          $(_this).popover("hide");
        }
      }, 200);
    });
  })
  .on("mouseleave", function() {
    const _this = this;
    setTimeout(function() {
      if (!$(".popover:hover").length && !$(_this).is(":hover")) {
        $(_this).popover("hide");
      }
    }, 200);
  });

$('.contacts-btn').popover({
  trigger: "click",
  html: true,
  content: function() {
    return $(this)
      .next(".popover-content")
      .html();
  }
});

// Header
$('[data-toggle="header-submenu"]')
  .on("mouseenter", function() {
    const _this = $(this);
    showHeaderSubmenu();
    $(".header-submenu").on("mouseleave", function() {
      setTimeout(function() {
        if (!_this.is(":hover")) {
          hideHeaderSubmenu();
        }
      }, 400);
    });
  })
  .on("mouseleave", function() {
    const _this = $(this);
    setTimeout(function() {
      if (!$(".header-submenu:hover").length && !_this.is(":hover")) {
        hideHeaderSubmenu();
      }
    }, 400);
  });

function showHeaderSubmenu() {
  $(".header").addClass("submenu__active");
  $(".header-submenu").fadeIn(300);
}

function hideHeaderSubmenu() {
  $(".header").removeClass("submenu__active");
  $(".header-submenu").fadeOut(300);
}

$(".header-controls__search").click(function() {
  if (!$(this).hasClass("active")) {
    const _this = $(this);
    $(this).addClass("active");
    $(".header-search").fadeIn(300);
    $("#headerSearch").focus();
    $(window).on("mouseup", function(e) {
      if (
        !$(e.target).is(".header-search") &&
        !$(".header-search").has(e.target).length &&
        !$(e.target).is(_this) &&
        !_this.has(e.target).length
      ) {
        _this.removeClass("active");
        $(".header-search").fadeOut(300);
      }
    });
  } else {
    $(this).removeClass("active");
    $(".header-search").fadeOut(300);
  }
});

$("[data-header-compensate]").each(function() {
  const headerHeight = $(".header").outerHeight();
  $(this).css({ marginTop: headerHeight + "px" });
});

// Side-menu
$(".side-menu-toggle").click(function() {
  const menu = $("#sideMenu");
  if (!menu.hasClass("active")) {
    openSideMenu();
  }
});

$(".side-menu-close").click(function() {
  closeSideMenu();
});

$("[data-slideout-toggle]").click(function() {
  $("#sideMenu").addClass("slideout-active");
  $(".side-menu-slideout__close").one("click", function() {
    $("#sideMenu").removeClass("slideout-active");
  });
});

function openSideMenu() {
  $("#sideMenu").addClass("active");
  $("body").addClass("fixed");
}

function closeSideMenu() {
  $("#sideMenu").removeClass("active slideout-active");
  $("body").removeClass("fixed");
}

// Cookies
(function() {
  if (!localStorage.cookiesAccept) {
    $(".cookies").show();
    $("[data-cookies-close]").click(function() {
      localStorage.cookiesAccept = true;
      $(".cookies").hide();
    });
  } else {
    return false;
  }
})();

// Square blocks
$("[data-square]").each(function() {
  const _this = $(this);
  $(window).on("load resize", function() {
    const width = _this.outerWidth();
    _this.height(width);
  });
});

// Responsive backgrounds
$("[data-responsive-bg]").each(function() {
  const _this = $(this);
  const srcDesk = _this.data("src-desk");
  const srcMobile = _this.data("src-mobile");
  $(window).on("load resize", function() {
    if (window.innerWidth < 767) {
      _this.css({ backgroundImage: `url('${srcMobile}')` });
    } else {
      _this.css({ backgroundImage: `url('${srcDesk}')` });
    }
  });
});
