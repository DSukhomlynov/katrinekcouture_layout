// Add to wish list
$(".add-to-wish").click(function() {
  const addText = $(this).data("add");
  const removeText = $(this).data("remove");
  if (!$(this).hasClass("voted")) {
    $(this).addClass("voted");
    $.notify({
      title: addText
    });
  } else {
    $(this).removeClass("voted");
    $.notify({
      title: removeText
    });
  }
});

// Filters controls
$(".filter-toggle").click(function() {
  const toggle = $(this);
  const filter = $(this).closest(".filter");
  if (!filter.hasClass("active")) {
    filter
      .addClass("active")
      .siblings()
      .removeClass("active");
    $(".filter-container").hide();
    toggle
      .next(".filter-container")
      .slideDown(400);
    $(window).on("mousedown", function(e) {
      const target = $(e.target);
      if (!target.is(filter) && !filter.has(target).length) {
        filter.removeClass("active");
        toggle
          .next(".filter-container")
          .slideUp(400);
      }
    })
  } else {
    filter.removeClass("active");
    toggle
      .next(".filter-container")
      .slideUp(400);
  }
});
$(".filter-clear").click(function() {
  const filter = $(this).closest(".filter");
  const inputs = filter.find("input[type='checkbox']");
  inputs.prop("checked", false);
});

// Range-slider controls
$("#slider").slider({
  range: true,
  min: 200,
  max: 2885,
  values:[200, 2885],
  slide: function(event, ui) {
    $(".mse2_number_inputs input").eq(0).val(ui.values[0]);
    $(".mse2_number_inputs input").eq(1).val(ui.values[1]);
  }
});

$(".mse2_number_inputs input").eq(0).val($("#slider").slider("values", 0));
$(".mse2_number_inputs input").eq(1).val($("#slider").slider("values", 1));

$(".mse2_number_inputs input").keyup(function() {
  $("#slider").slider("values", $(this).index(), $(this).val());
})
