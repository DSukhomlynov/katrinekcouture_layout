// Main-banner
$(document).ready(function() {
  const mainSwiper = new Swiper(".main-banner-swiper", {
    effect: "fade",
    speed: 700,
    watchOverflow: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    }
  });
  const parallaxSwiper = new Swiper(".parallax-swiper", {
    slidesPerView: 1,
    spaceBetween: 6,
    watchOverflow: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 10,
      }
    }
  })
});
