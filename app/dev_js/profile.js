$("select").styler();

// Save changes form submit
$("#profileForm").submit(function(e) {
  e.preventDefault();
  const _this = this;
  // поместить в callback обработчика отправки формы
  $.fancybox.open({
    src: "#saveSuccess",
    type: "inline",
    touch: false,
    smallBtn: true,
    btnTpl: {
      smallBtn: '<i class="popup-close icon-close" data-fancybox-close></i>'
    },
    afterClose: function() {
      _this.reset();
    }
  });
});

// Datepicker init
$("#datePicker").daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  minYear: 1920,
  maxYear: parseInt(moment().format("YYYY"), 10),
  locale: {
    format: "YYYY-MM-DD",
    monthNames: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ]
  }
});
