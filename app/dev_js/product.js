// Gallery-slider init
$(document).ready(function() {
  const galleryTop = new Swiper(".product-gallery-top", {
    slidesPerView: 1,
    watchOverflow: true,
    effect: "fade",
    speed: 700,
    preloadImages: false,
    lazy: {
      loadOnTransitionStart: true
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    pagination: {
      el: ".swiper-pagination",
      type: "fraction"
    },
    on: {
      slideChange: function() {
        const slideIndex = this.activeIndex;
        galleryThumbs.slideTo(slideIndex);
        $(galleryThumbs.slides[slideIndex])
          .addClass("active")
          .siblings()
          .removeClass("active");
      }
    },
  });
  const galleryThumbs = new Swiper(".product-gallery-thumbs", {
    slidesPerView: 4,
    spaceBetween: 20,
    watchOverflow: true,
    freeMode: true,
    on: {
      init: function() {
        $(this.slides[0]).addClass("active");
      },
      click: function() {
        const slide = this.clickedSlide;
        const slideIndex = this.clickedIndex;
        $(slide)
          .addClass("active")
          .siblings()
          .removeClass("active");
        galleryTop.slideTo(slideIndex);
      }
    }
  });
});

// Size-guide popup
$(".js-popup-size").fancybox({
  src: "#popupSize",
  type: "inline",
  touch: false,
  smallBtn: true,
  btnTpl: {
    smallBtn: '<i class="popup-close icon-close" data-fancybox-close></i>'
  },
})

// Select-size dropdown
$(".dropdown-head").click(function() {
  const head = $(this);
  const body = head.next(".dropdown-body");
  const dropdown = head.closest(".dropdown");
  const input = $("#size");
  const alert = $("#sizeAlert");
  dropdown.toggleClass("active");
  alert.hide();
  $(window).one("mousedown", function(e) {
    const target = $(e.target);
    if (!target.is(head) && dropdown.hasClass("active")) {
      if (target.is(".size-select__item")) {
        const val = target.text();
        head.text(val);
        input.val(val);
      }
      head.closest(".dropdown").toggleClass("active");
    }
  });
});

// Add-to-cart btn
$("#addToCart").click(function() {
  const addText = $(this).data("add");
  if (!$(".product-open").hasClass("no-size")) {
    const input = $("#size");
    const alert = $("#sizeAlert");
    if (!input.val()) {
      const sizeAlert = $(this).data("size-alert");
      $.notify({
        title: sizeAlert
      });
      alert.show();
      return false;
    } else {
      $.notify({
        title: addText
      });
      $(".cart-full-content").removeClass("d-none");
      $(".cart-empty-content").addClass("d-none");
    }
  } else {
    $.notify({
      title: addText
    });
    $(".cart-full-content").removeClass("d-none");
    $(".cart-empty-content").addClass("d-none");
  }
});

// Add-to-wish btn
$("#addToWish").click(function() {
  $(this).toggleClass("voted");
  if ($(this).hasClass("voted")) {
    const addText = $(this).data("add");
    $.notify({
      title: addText
    });
  } else {
    const removeText = $(this).data("remove");
    $.notify({
      title: removeText
    });
  }
});
