// Count input
$(".cart__form__item-count").change(function() {
  if(!$(this).val()) {
    $(this).val("1");
  }
})

// Items delete
$(".cart__form__item-delete").click(function() {
  const item = $(this).closest(".cart__form__item");
  const text = $(this).data("remove");
  item.remove();
  $.notify({
    title: text
  })
})

// Form submit (layout only)
$(".cart__form").submit(function(e) {
  e.preventDefault();
  window.location = "order.html"
})
