// Show registration / hide login
$("#showRegistration").click(function() {
  $("#login").hide();
  $("#registration").show();
});

// Restore password popup
$(".js-popup-restore").fancybox({
  src: "#restorePassword",
  type: "inline",
  touch: false,
  smallBtn: true,
  btnTpl: {
    smallBtn: '<i class="popup-close icon-close" data-fancybox-close></i>'
  }
});

// Inputs validation
function checkRequiredField(input) {
  if (!input.val()) {
    input
      .closest(".form-field")
      .find(".input-alert")
      .show();
    return false;
  } else {
    input
      .closest(".form-field")
      .find(".input-alert")
      .hide();
    return true;
  }
}
function checkPasswordConfirm() {
  const passwordFiled = $("input[name='password']");
  const repasswordField = $("input[name='repassword']");
  const password = passwordFiled.val();
  const repassword = repasswordField.val();
  if (!repassword || repassword !== password) {
    repasswordField
      .closest(".form-field")
      .find(".input-alert")
      .show();
    return false;
  } else {
    repasswordField
      .closest(".form-field")
      .find(".input-alert")
      .hide();
    return true;
  }
}

$("input")
  .not("input[name='repassword']")
  .blur(function() {
    checkRequiredField($(this));
  });
$("input[name='repassword']").blur(function() {
  checkPasswordConfirm();
});

// Restore password form submit
$("#restorePasswordForm").submit(function(e) {
  e.preventDefault();
  const _this = this;
  let isValid = true;
  $(this).find("input").each(function() {
    const validField = checkRequiredField($(this));
    if (!validField) {
      isValid = false;
    }
  })
  if (isValid) {
    // поместить в callback обработчика отправки формы
    $.fancybox.open({
      src: "#restorePasswordSuccess",
      type: "inline",
      touch: false,
      smallBtn: true,
      btnTpl: {
        smallBtn: '<i class="popup-close icon-close" data-fancybox-close></i>'
      },
      afterClose: function() {
        _this.reset();
        $.fancybox.close(true);
      }
    });
  }
});

// Login form submit
$("#loginForm").submit(function(e) {
  e.preventDefault();
  let isValid = true;
  $(this).find("input").each(function() {
    const validField = checkRequiredField($(this));
    if (!validField) {
      isValid = false;
    }
  })
  if (isValid) {
    // заменить на обработчик отправки формы
    window.location = "index.html";
  }
});

// Registration form submit
$("#registrationForm").submit(function(e) {
  e.preventDefault();
  let isValid = true;
  $(this).find("input").each(function() {
    const validField = checkRequiredField($(this));
    if (!validField) {
      isValid = false;
    }
  })
  if (!checkPasswordConfirm()) {
    isValid = false;
  }
  if (isValid) {
    // заменить на обработчик отправки формы
    window.location = "login_success.html";
  }
});
