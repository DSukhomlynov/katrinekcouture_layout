let currentTab = 0;
function showCurrentTab(n) {
  $(".order-form__title li")
    .eq(n)
    .addClass("active")
    .siblings()
    .removeClass("active");
  $(".tab")
    .eq(n)
    .show()
    .siblings()
    .hide();
}
showCurrentTab(currentTab);

// Select input init
$("select").styler({
  onSelectClosed: function() {
    if (!$("#countrySelect").val()) {
      $(this)
        .closest(".form-field")
        .find(".input-alert")
        .show();
    } else {
      $(this)
        .closest(".form-field")
        .find(".input-alert")
        .hide();
    }
  }
});

// Form inputs validation
$("#firstTab input").blur(function() {
  if (!$(this).val()) {
    $(this)
      .closest(".form-field")
      .find(".input-alert")
      .show();
  } else {
    $(this)
      .closest(".form-field")
      .find(".input-alert")
      .hide();
  }
});

// Next button controls
$("#next").click(function() {
  let isValid = true;
  $("#firstTab input, #firstTab select").each(function() {
    if (!$(this).val()) {
      $(this)
        .closest(".form-field")
        .find(".input-alert")
        .show();
      isValid = false;
    }
  });
  if (isValid) {
    currentTab++;
    showCurrentTab(currentTab);
  } else {
    return false;
  }
});

// Back button controls
$("#back").click(function() {
  currentTab--;
  showCurrentTab(currentTab);
});

// Form validation
$("#orderForm").submit(function(e) {
  e.preventDefault();
  window.location = "order_success.html";
})
